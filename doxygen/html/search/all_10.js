var searchData=
[
  ['readme_2emd',['README.md',['../_r_e_a_d_m_e_8md.html',1,'']]],
  ['recherche_2ec',['recherche.c',['../recherche_8c.html',1,'']]],
  ['recherche_5ferreur',['RECHERCHE_ERREUR',['../base__descripteur_8h.html#a82ec226b11a97f28a0b2af5e9b63f017',1,'base_descripteur.h']]],
  ['recherche_5fet_5fdestruction',['recherche_et_destruction',['../xml__index_8c.html#acf5616eb7d98ddc1b5beb36910b9eb37',1,'xml_index.c']]],
  ['recherche_5fok',['RECHERCHE_OK',['../base__descripteur_8h.html#a407e589c3cbd06e583c6b1b71d4adbd3',1,'base_descripteur.h']]],
  ['recherchemot_5ftexte',['rechercheMot_texte',['../module__texte_2header_8h.html#a2e459d0e9162af6af1af51391426f647',1,'rechercheMot_texte(Table_Index a, char *mot):&#160;recherche.c'],['../recherche_8c.html#a2e459d0e9162af6af1af51391426f647',1,'rechercheMot_texte(Table_Index a, char *mot):&#160;recherche.c']]],
  ['rechercheparcritere_5fimage',['rechercheParCritere_image',['../moteur_8c.html#aa502028a32c6de79234f20a65913fae0',1,'rechercheParCritere_image(RGB couleurDominante, FILE *fichiersSimilaires, int seuilSimilarite):&#160;moteur.c'],['../moteur_8h.html#aa502028a32c6de79234f20a65913fae0',1,'rechercheParCritere_image(RGB couleurDominante, FILE *fichiersSimilaires, int seuilSimilarite):&#160;moteur.c']]],
  ['rechercheparcritere_5ftexte',['rechercheParCritere_texte',['../module__texte_2header_8h.html#adc886f2eda48cce335a65999c2f16b8f',1,'rechercheParCritere_texte(char *mot, char *fichiersSimilaires[], int *nbF):&#160;Header.h'],['../recherche_8c.html#ae03b630a8220244e42ad04e7509222cf',1,'rechercheParCritere_texte(char *mot, char **fichiersSimilaires, int *nbF):&#160;recherche.c']]],
  ['recherchepardocument_5fnb',['rechercheParDocument_NB',['../moteur_8c.html#a87cefab1363ade0c72050cde63e8c16b',1,'rechercheParDocument_NB(char *cheminVersDocument, FILE *fichiersSimilaires, int seuilSimilarite):&#160;moteur.c'],['../moteur_8h.html#a87cefab1363ade0c72050cde63e8c16b',1,'rechercheParDocument_NB(char *cheminVersDocument, FILE *fichiersSimilaires, int seuilSimilarite):&#160;moteur.c']]],
  ['recherchepardocument_5frgb',['rechercheParDocument_RGB',['../moteur_8c.html#a1f2dce121034a0c9860ba79766189e87',1,'rechercheParDocument_RGB(char *cheminVersDocument, FILE *fichiersSimilaires, int seuilSimilarite):&#160;moteur.c'],['../moteur_8h.html#a1f2dce121034a0c9860ba79766189e87',1,'rechercheParDocument_RGB(char *cheminVersDocument, FILE *fichiersSimilaires, int seuilSimilarite):&#160;moteur.c']]],
  ['recherchepardocument_5ftexte',['rechercheParDocument_texte',['../module__texte_2header_8h.html#aa7cdbaa5a318bd6188d4099fac5212a9',1,'rechercheParDocument_texte(char *cheminVersDocument, char *fichiersSimilaires[], double seuilSimilarite):&#160;recherche.c'],['../recherche_8c.html#aa7cdbaa5a318bd6188d4099fac5212a9',1,'rechercheParDocument_texte(char *cheminVersDocument, char *fichiersSimilaires[], double seuilSimilarite):&#160;recherche.c']]],
  ['rechercher_5fdesc_5faudio',['rechercher_DESC_AUDIO',['../base__descripteur_8c.html#a6937b8106276a701a1fba48c17283989',1,'rechercher_DESC_AUDIO(char *source, unsigned int n, double threshold, int *code):&#160;base_descripteur.c'],['../base__descripteur_8h.html#a6937b8106276a701a1fba48c17283989',1,'rechercher_DESC_AUDIO(char *source, unsigned int n, double threshold, int *code):&#160;base_descripteur.c']]],
  ['recuperer_5fnouvel_5fid_5fvalide_5faudio',['recuperer_nouvel_id_valide_AUDIO',['../base__descripteur_8c.html#a4ffa2addfad90d96b9481d4813cd4005',1,'recuperer_nouvel_id_valide_AUDIO():&#160;base_descripteur.c'],['../base__descripteur_8h.html#a4ffa2addfad90d96b9481d4813cd4005',1,'recuperer_nouvel_id_valide_AUDIO():&#160;base_descripteur.c']]],
  ['red',['red',['../struct_r_g_b__t.html#a6761340706096510fd89edca40a63b9b',1,'RGB_t']]],
  ['res_5feval_5faudio',['RES_EVAL_AUDIO',['../struct_r_e_s___e_v_a_l___a_u_d_i_o.html',1,'RES_EVAL_AUDIO'],['../module__audio_2descripteur_8h.html#ab96b1b5f8fa70503cc676245070dfe02',1,'RES_EVAL_AUDIO():&#160;descripteur.h']]],
  ['res_5frecherche_5faudio',['RES_RECHERCHE_AUDIO',['../struct_r_e_s___r_e_c_h_e_r_c_h_e___a_u_d_i_o.html',1,'RES_RECHERCHE_AUDIO'],['../base__descripteur_8h.html#ad7b1c03b9bde223a9a3b7cd7dd1810af',1,'RES_RECHERCHE_AUDIO():&#160;base_descripteur.h']]],
  ['resultat_5fevalutation_5ft',['Resultat_Evalutation_t',['../struct_resultat___evalutation__t.html',1,'']]],
  ['resultat_5frecheche_5ft',['Resultat_Recheche_t',['../struct_resultat___recheche__t.html',1,'']]],
  ['resultats',['resultats',['../struct_resultat___recheche__t.html#a79cba231fd4a669ceab868f56977c45c',1,'Resultat_Recheche_t']]],
  ['rgb',['RGB',['../controle__descripteur_8h.html#af27a8b335a31550794ac7280237288c4',1,'controle_descripteur.h']]],
  ['rgb_5ft',['RGB_t',['../struct_r_g_b__t.html',1,'']]]
];
