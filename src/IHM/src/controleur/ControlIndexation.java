package controleur;

import java.io.File;
import java.util.List;

import modele.BDIndexer;
import modele.TypeDocument;

public class ControlIndexation {

	private BDIndexer bdIndexer = BDIndexer.getInstance();

	public String lancerIndexationFichier(TypeDocument typeDocument, String chemin) {
		// SIMULE DES RESULTATS
		bdIndexer.ajouterFichierIndexe(typeDocument, chemin);
		return chemin;
	}

	public List<String> lancerIndexationDossier(TypeDocument typeDocument, String chemin) {
		// SIMULE DES RESULTATS
		String extension;
		switch (typeDocument) {
		case SON:
			extension = ".wav";
			break;
		case IMAGE:
			extension = ".txt";
			break;
		default:
			extension = ".txt";
			break;
		}
		for (int i = 0; i < 20; i++) { // Simulation
			lancerIndexationFichier(typeDocument, chemin + "/" + i + extension);
		}
		return bdIndexer.getFichiersIndexe(typeDocument);
	}

	public boolean verifierCheminFichier(String chemin) {
		File file = new File(chemin);
		return file.exists();
	}

	public boolean verifierCheminDossier(String chemin) {
		File file = new File(chemin);
		return file.exists();
	}
}
